﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.DynamicProxy;

namespace DynamicProxyDemo
{
    class Program
    {
        private static void Main(string[] args)
        {
            var notifier = new Notifier();
            notifier.Send("Hello world");
            var proxyGenerator = new ProxyGenerator();
            var proxyNotifier = proxyGenerator
            .CreateClassProxy<Notifier>(
            new MyInterceptorAspect());
            proxyNotifier.Send("Hello world");
            //Output
            /*
             * Hello world
             * Interceptor before the method run
             * Hello world
             * Interceptor after the method run
             */
        }
    }
}
