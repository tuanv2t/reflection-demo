﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.DynamicProxy;

namespace DynamicProxyDemo
{
    public class MyInterceptorAspect: IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            Console.WriteLine("Interceptor before the method run");
            invocation.Proceed();
            Console.WriteLine("Interceptor after the method run");
        }
    }
}
