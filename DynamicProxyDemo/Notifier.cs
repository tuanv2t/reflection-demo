﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicProxyDemo
{
    public class Notifier
    {
        public virtual void Send(string msg)
        {
            Console.WriteLine("Sending: {0}", msg);
        }
    }
}
